from datetime import datetime

from django.db import models
from jsonfield import JSONField


class MonNotifierQueue(models.Model):
    id = models.BigAutoField(primary_key=True)
    host = models.CharField(max_length=250)
    data = JSONField()
    queued_since = models.DateTimeField(null=False, default=datetime.now)

    class Meta:
        db_table = "mon_notifier_queue"
