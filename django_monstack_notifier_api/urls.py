from django.urls import path, include
from tastypie.api import Api

from django_monstack_notifier_api.django_monstack_notifier_api.api.resources import *

v1_api = Api(api_name="v1")
v1_api.register(NotificationResource())

urlpatterns = [
    path("", include(v1_api.urls)),
]
