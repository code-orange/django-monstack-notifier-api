from django.http import HttpResponse
from tastypie.resources import Resource
from tastypie.serializers import Serializer

from django_cdstack_deploy.django_cdstack_deploy.authentification import (
    HostApiKeyAuthentication,
)
from django_monstack_notifier_api.django_monstack_notifier_api.models import *


class NotificationResource(Resource):
    class Meta:
        always_return_data = True
        allowed_methods = ["post"]
        serializer = Serializer()
        authentication = HostApiKeyAuthentication()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_create(self, bundle, **kwargs):
        bundle.related_obj = None
        bundle.obj = None

        host = bundle.request.user

        notification_data = list()
        notification_data.append("check_type")
        notification_data.append("long_date_time")
        notification_data.append("hostname")
        notification_data.append("state")
        notification_data.append("notification_type")

        for value in notification_data:
            if value not in bundle.data:
                return HttpResponse(status=422)

        new_entry = MonNotifierQueue(host=host.universal_id, data=bundle.data)
        new_entry.save(force_insert=True)

        return bundle

    def dehydrate(self, bundle):
        return bundle
